import 'package:flutter/material.dart';

final Map<String, IconData> _icons = <String, IconData>{
  'accessibility_new': Icons.accessibility_new,
  'ac_unit': Icons.ac_unit,
  'android': Icons.android,
  'healing': Icons.healing,
  'access_time': Icons.access_time,
  'monetization_on': Icons.monetization_on,
  'offline_bolt': Icons.offline_bolt,
  'title': Icons.title,
  'library_music': Icons.library_music
};

int getColorFromHex(String hexColor) {
  hexColor = hexColor.toUpperCase().replaceAll("#", "");
  if (hexColor.length == 6) {
    hexColor = "FF" + hexColor;
  }
  return int.parse(hexColor, radix: 16);
}

Icon getIcon(String iconName, String hexColor) {
  return Icon(_icons[iconName], color: Color(getColorFromHex(hexColor)));
}
