import 'dart:convert';
import 'dart:core';

import 'package:http/http.dart' as http;

import 'package:tarea_listas_ttm_jcsp/src/models/mis_heroes_model.dart';

class TestService {
  final String _baseUrl = 'https://www.hectoralvarez.dev/icc727/';

  Map<String, String> _headers = {
    'Content-Type': 'application/json',
    'Authorization': 'mi\$up34Token'
  };

  Future<List<dynamic>> getLista() async {
    final response =
        await http.get(this._baseUrl + 'heroes.json', headers: this._headers);

    print("Codigo statis del GET: ${response.statusCode}");
    if (response.statusCode == 200) {
      print("Datos response recibidos!");
      List<dynamic> heroesMap =
          MisHeroesModel.fromJson(json.decode(response.body)).heroes;
      return heroesMap;
    } else {
      return <dynamic>[];
    }
  }
}
